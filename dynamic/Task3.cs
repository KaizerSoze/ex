﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace dynamic
{
    static class Task3
    {
        public static int Lucky_N_ticket(int N)
        {
            int[] array = new int[9*N+1];
            double len = Math.Pow(10, N);
            for (int i = 0; i <len; i++)
            {
                int sum=0;
                int temp = i;
                for(int j=0;j<N;j++)
                {
                    sum += temp % 10;
                    temp /= 10;

                }
                array[sum]++;
            }
            int n = 0;
            for (int i = 0; i < array.Length; i++)
                n += array[i] * array[i];
            return n;
        }
    }
}
