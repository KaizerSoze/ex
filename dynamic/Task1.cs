﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace dynamic
{
    static class Task1
    {
        public static double Sum(int k)
        {
            double sum = 1;
            int factorial = 1;
            for (int i = 0; i < k-1; i++)
            {
                factorial *= (factorial+1);
                sum += 1 / (float)factorial;
            }

            return sum;
        }
    }
}
