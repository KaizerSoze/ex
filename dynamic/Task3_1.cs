﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace dynamic
{
    class Task3_1
    {
        public static double Sum_of_squares(int k)
        {
            double sum = 0;
            double start = 0, finish = Math.Pow(10, k);
            for (int i = k - 1; i > 0; i--)
            {
                start += Math.Pow(10, i);
            }
            for (double i = 1 + start; i < finish; i++)
            {
                string str = Convert.ToString(i);
                if (!str.Contains('0'))
                {
                    sum += i * i;
                }
            }
            return sum;
        }
    }
}
