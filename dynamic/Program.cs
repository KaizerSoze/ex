﻿using System;

namespace dynamic
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Task 1: "+Task1.Sum(3));
            Console.WriteLine("Task 2: " + Task2.Lucky_ticket());
            Console.WriteLine("Task 3: " + Task3.Lucky_N_ticket(4));
            Console.WriteLine("Task 3_1: " + Task3_1.Sum_of_squares(4));
            Console.WriteLine("Task 3_2_a: " + Task_3_2_a.Number(3,1));
            Console.ReadLine();
        }
    }
}
