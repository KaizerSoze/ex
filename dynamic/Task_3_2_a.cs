﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace dynamic
{
    static class Task_3_2_a
    {
        public static int Number(int n, int k)
        {
            int result = 0;
            double finish = Math.Pow(10, n);
            for (int i = 0; i < finish; i++)
            {
                string str = Convert.ToString(i);
                int sum = 0;
                for (int j = 0; j < str.Length; j++)
                {
                    sum += Convert.ToInt32(new string(str[j], 1));
                }

                if (sum == k)
                {
                    result++;
                }
            }

            return result;
        }
    }
}
