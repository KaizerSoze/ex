﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace dynamic
{
    static class Task2
    {
        public static int Lucky_ticket()
        {
            const int length = 28;
            int[] array = new int[length];
            for (int i = 0; i < 10; i++)
                for (int j = 0; j < 10; j++)
                    for (int k = 0; k < 10; k++)
                        array[i + j + k]++;
            int n = 0;
            for (int i = 0; i < 28; i++)
                n += array[i] * array[i];
            return n;
        }
    }
}
